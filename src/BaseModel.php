<?php

/**
 * Class BaseModel
 * @package core\base
 */
class BaseModel implements JsonSerializable, ArrayAccess
{
	/** @var array object properties storage */
	protected $_attributes = [];
	/** @var array list with "attribute name => class name" pares for casting */
	protected $_cast = [];

	/**
	 * Model constructor
	 * @param array|null $data associated array with initial data
	 * @see load
	 */
	public function __construct($data = null)
	{
		$this->load($data);
	}

	/**
	 * Returns class name
	 * @return string
	 */
	public static function className()
	{
		return get_called_class();
	}

	/**
	 * Apply data on object attributes
	 * Available automatic casts of properties that have 'class' key or persist in $this->_cast property by creating
	 * of instance with passing value to constructor
	 * @param array $data
	 */
	public function load($data)
	{
		if (!is_array($data)) {
			return;
		}

		foreach ($data as $name => $value)
		{
			if (is_array($value)) {
				if (isset($this->_cast[$name])) {
					$className = $this->_cast[$name];
				} elseif (isset($value['class'])) {
					$className = $value['class'];
				}
				if (!empty($className)) {
					unset($value['class']);
					/** @noinspection PhpVariableVariableInspection */
					$value = new $className($value);
				}
			}

			$this->$name = $value;
		}

		$this->init();
	}

	/**
	 * Calls after attributes loaded
	 */
	public function init() {}

	/**
	 * Getter for magic properties
	 * @param string $name property name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		if (array_key_exists($name, $this->_attributes)) {
			return $this->_attributes[$name];
		}

		$getter = 'get' . ucfirst($name);
		if (!method_exists($this, $getter)) {
			throw new Exception('There is no getter for "' . $name . '" in ' . self::className());
		}

		return $this->$getter();
	}

	/**
	 * Setter for magic properties
	 * @param string $name property name
	 * @param mixed $value new value
	 * @throws Exception
	 */
	public function __set($name, $value)
	{
		if (array_key_exists($name, $this->_attributes)) {
			$this->_attributes[$name] = $value;
			return;
		}

		$setter = 'set' . ucfirst($name);
		if (!method_exists($this, $setter)) {
			throw new Exception('There is no setter for "' . $name . '" in ' . self::className());
		}

		$this->$setter($value);
	}

	/**
	 * @inheritdoc
	 */
	function __isset($name)
	{
		return array_key_exists($name, $this->_attributes);
	}

	/**
	 * {@inheritDoc}
	 */
	function jsonSerialize()
	{
		return $this->_attributes;
	}

	/**
	 * {@inheritDoc}
	 */
	public function offsetSet($offset, $value)
	{
		$this->_attributes[$offset] = $value;
	}

	/**
	 * {@inheritDoc}
	 */
	public function offsetExists($offset)
	{
		return isset($this->_attributes[$offset]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function offsetUnset($offset)
	{
		$this->_attributes[$offset] = null;
	}

	/**
	 * {@inheritDoc}
	 */
	public function offsetGet($offset)
	{
		return $this->_attributes[$offset];
	}

	/**
	 * Export objects and nested properties to array
	 * @return array
	 */
	public function toArray()
	{
		return array_map(function ($item) {
			return $item instanceof BaseModel ? $item->toArray() : $item;
		}, $this->_attributes);
	}
}
