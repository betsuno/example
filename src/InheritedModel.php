<?php

/**
 * Class InheritedModel
 * @property NestedModel $test
 * @property int $num
 */
class InheritedModel extends BaseModel
{
	/** {@inheritDoc} */
	protected $_attributes = [
		'num'  => 0,
		'test' => null,
	];
	/** {@inheritDoc} */
	protected $_cast = ['test' => 'NestedModel'];
}