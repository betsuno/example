<?php

/**
 * Class NestedModel
 * @property string $name
 * @property string $bar
 */
class NestedModel extends BaseModel
{
	/** {@inheritDoc} */
	protected $_attributes = [
		'name' => null,
		'bar'  => null,
	];
}