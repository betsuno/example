<?php

require 'src/BaseModel.php';
require 'src/NestedModel.php';
require 'src/InheritedModel.php';

$model = new InheritedModel([
	'num' => 13,
	'test' => [
		'name' => 'foo',
		'bar' => 'baz',
	],
]);

echo json_encode($model, JSON_PRETTY_PRINT) . PHP_EOL;
var_dump($model->toArray());